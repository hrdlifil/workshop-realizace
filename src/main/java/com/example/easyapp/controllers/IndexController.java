package com.example.easyapp.controllers;

import com.example.easyapp.repository.OsobaRepository;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class IndexController {

    private OsobaRepository osobaRepository;

    @GetMapping("/")
    public String getIndex(){
        return "Ahoj světe z CZM";
    }

    @GetMapping("/osoba")
    public String getOsoba(){
        return this.osobaRepository.findAll().get(0).getName();
    }
}
